(kicad_symbol_lib (version 20211014) (generator kicad_symbol_editor)
  (symbol "IRM-H6xxT" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "IC" (id 0) (at -5.08 5.08 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "IRM-H6xxT" (id 1) (at -5.08 -5.08 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Mondev:IRM-H6XXT" (id 2) (at -5.08 -7.62 0)
      (effects (font (size 1.27 1.27)) (justify left) hide)
    )
    (property "Datasheet" "https://datasheet.lcsc.com/szlcsc/2010221806_Everlight-Elec-IRM-H638T-TR2-DX_C390031.pdf" (id 3) (at -5.08 -10.16 0)
      (effects (font (size 1.27 1.27)) (justify left) hide)
    )
    (property "LCSC" "C390031" (id 4) (at -5.08 -12.7 0)
      (effects (font (size 1.27 1.27)) (justify left) hide)
    )
    (property "ki_description" "Infrared Receiver Module" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "IRM-H6xxT_0_0"
      (rectangle (start -5.08 3.81) (end 5.08 -3.81)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type background))
      )
      (pin power_in line (at -7.62 0 0) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -7.62 -2.54 0) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 7.62 2.54 180) (length 2.54)
        (name "OUT" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -7.62 2.54 0) (length 2.54)
        (name "VCC" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "MAX17048" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at 0 10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "MAX17048" (id 1) (at 0 -10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "Package_DFN_QFN:DFN-8-1EP_2x2mm_P0.5mm_EP0.7x1.3mm" (id 2) (at -9.525 -10.16 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://datasheets.maximintegrated.com/en/ds/MAX17048-MAX17049.pdf" (id 3) (at -9.525 -10.16 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "maxim integrated modelgauge" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "Fuel gauge chip with integrated current sensing resistor" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "MAX17048_0_0"
      (pin power_in line (at -8.89 -1.27 0) (length 2.54)
        (name "CTG" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -8.89 3.81 0) (length 2.54)
        (name "CELL" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -8.89 6.35 0) (length 2.54)
        (name "VDD" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -8.89 -3.81 0) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin open_collector line (at 8.89 6.35 180) (length 2.54)
        (name "~{ALRT}" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin input line (at 8.89 -6.35 180) (length 2.54)
        (name "QSTRT" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 8.89 -1.27 180) (length 2.54)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 8.89 1.27 180) (length 2.54)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "8" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -8.89 -6.35 0) (length 2.54)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "9" (effects (font (size 1.27 1.27))))
      )
    )
    (symbol "MAX17048_0_1"
      (rectangle (start -6.35 8.89) (end 6.35 -8.89)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
  )
  (symbol "ST25DV04K-JF" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at -12.7 10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "ST25DV04K-JF" (id 1) (at 7.62 -10.16 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "Package_DFN_QFN:DFN-8-1EP_3x2mm_P0.5mm_EP1.3x1.5mm" (id 2) (at -9.525 -10.16 0)
      (effects (font (size 1.27 1.27) italic) hide)
    )
    (property "Datasheet" "https://www.st.com/resource/en/datasheet/st25dv04k.pdf" (id 3) (at 0 -15.24 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "ST25DV-I2C series Dynamic NFC Tag" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "13.56 MHz long-range interface dynamic NFC/RFID tag, Package: UFDFPN-8, EEPROM: 5 kb, Voltage: 1,8..5,5V, I/O pins: 8" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "ST25DV04K-JF_0_1"
      (rectangle (start -13.97 8.89) (end 13.97 -8.89)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type background))
      )
    )
    (symbol "ST25DV04K-JF_1_1"
      (pin input line (at -17.78 -6.35 0) (length 3.81)
        (name "VEH" (effects (font (size 1.016 1.016))))
        (number "1" (effects (font (size 1.016 1.016))))
      )
      (pin passive line (at -17.78 -3.81 0) (length 3.81)
        (name "AC0" (effects (font (size 1.016 1.016))))
        (number "2" (effects (font (size 1.016 1.016))))
      )
      (pin passive line (at -17.78 -1.27 0) (length 3.81)
        (name "AC1" (effects (font (size 1.016 1.016))))
        (number "3" (effects (font (size 1.016 1.016))))
      )
      (pin power_in line (at 17.78 -6.35 180) (length 3.81)
        (name "VSS" (effects (font (size 1.016 1.016))))
        (number "4" (effects (font (size 1.016 1.016))))
      )
      (pin bidirectional line (at -17.78 1.27 0) (length 3.81)
        (name "SDA" (effects (font (size 1.016 1.016))))
        (number "5" (effects (font (size 1.016 1.016))))
      )
      (pin input line (at -17.78 3.81 0) (length 3.81)
        (name "SCL" (effects (font (size 1.016 1.016))))
        (number "6" (effects (font (size 1.016 1.016))))
      )
      (pin output line (at -17.78 6.35 0) (length 3.81)
        (name "GPO" (effects (font (size 1.016 1.016))))
        (number "7" (effects (font (size 1.016 1.016))))
      )
      (pin power_in line (at 17.78 6.35 180) (length 3.81)
        (name "VCC" (effects (font (size 1.016 1.016))))
        (number "8" (effects (font (size 1.016 1.016))))
      )
    )
  )
  (symbol "Sensirion_SGP30" (in_bom yes) (on_board yes)
    (property "Reference" "U" (id 0) (at 0 6.35 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "Sensirion_SGP30" (id 1) (at 0 -8.89 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "Mondev:Sensirion_SGP30" (id 2) (at 0 -11.43 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://sensirion.com/resource/datasheet/sgp30" (id 3) (at 2.54 -13.97 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "Gas TVOC" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "Sensirion Gas Sensor Module for TVOC and Indoor Air Quality" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "Sensirion_SGP30_0_1"
      (rectangle (start -5.08 5.08) (end 5.08 -5.08)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
    )
    (symbol "Sensirion_SGP30_1_1"
      (pin power_in line (at -7.62 2.54 0) (length 2.54)
        (name "VDD" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -7.62 0 0) (length 2.54)
        (name "VSS" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at -7.62 -2.54 0) (length 2.54)
        (name "SDA" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin no_connect line (at 7.62 -2.54 180) (length 2.54)
        (name "R" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 7.62 0 180) (length 2.54)
        (name "VDDH" (effects (font (size 1.27 1.27))))
        (number "5" (effects (font (size 1.27 1.27))))
      )
      (pin bidirectional line (at 7.62 2.54 180) (length 2.54)
        (name "SCL" (effects (font (size 1.27 1.27))))
        (number "6" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 0 -7.62 90) (length 2.54)
        (name "VSS" (effects (font (size 1.27 1.27))))
        (number "7" (effects (font (size 1.27 1.27))))
      )
    )
  )
  (symbol "Soldfeld" (pin_names (offset 1.016)) (in_bom yes) (on_board yes)
    (property "Reference" "SF" (id 0) (at 0 1.27 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Value" "Soldfeld" (id 1) (at 0 -1.27 0)
      (effects (font (size 1.27 1.27)))
    )
    (property "Footprint" "Mondev:Solderarea" (id 2) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "" (id 3) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "sf solder feld" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "Solder Area" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "Soldfeld_0_1"
      (rectangle (start -5.08 -3.81) (end -5.08 3.81)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (rectangle (start -5.08 3.81) (end 5.08 3.81)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (rectangle (start 5.08 -3.81) (end -5.08 -3.81)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (rectangle (start 5.08 3.81) (end 5.08 -3.81)
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
    )
  )
  (symbol "TSOP75" (pin_names (offset 0) hide) (in_bom yes) (on_board yes)
    (property "Reference" "Q" (id 0) (at 5.08 1.27 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Value" "TSOP75" (id 1) (at 5.08 -1.27 0)
      (effects (font (size 1.27 1.27)) (justify left))
    )
    (property "Footprint" "Mondev:IR_Recv_Vishay_TSOP" (id 2) (at 12.192 -3.556 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "Datasheet" "https://www.vishay.com/docs/82494/tsop752.pdf" (id 3) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_keywords" "ir receiver" (id 4) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_description" "IR Receiver Module for Remote Control Systems" (id 5) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (property "ki_fp_filters" "LED*D5.0mm*Clear*" (id 6) (at 0 0 0)
      (effects (font (size 1.27 1.27)) hide)
    )
    (symbol "TSOP75_0_1"
      (polyline
        (pts
          (xy -1.905 1.27)
          (xy -2.54 1.27)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy -1.27 2.54)
          (xy -1.905 2.54)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy 0.635 0.635)
          (xy 2.54 2.54)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy -3.81 3.175)
          (xy -1.905 1.27)
          (xy -1.905 1.905)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy -3.175 4.445)
          (xy -1.27 2.54)
          (xy -1.27 3.175)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy 0.635 -0.635)
          (xy 2.54 -2.54)
          (xy 2.54 -2.54)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy 0.635 1.905)
          (xy 0.635 -1.905)
          (xy 0.635 -1.905)
        )
        (stroke (width 0.508) (type default) (color 0 0 0 0))
        (fill (type none))
      )
      (polyline
        (pts
          (xy 1.27 -1.778)
          (xy 1.778 -1.27)
          (xy 2.286 -2.286)
          (xy 1.27 -1.778)
          (xy 1.27 -1.778)
        )
        (stroke (width 0) (type default) (color 0 0 0 0))
        (fill (type outline))
      )
      (circle (center 1.27 0) (radius 2.8194)
        (stroke (width 0.254) (type default) (color 0 0 0 0))
        (fill (type none))
      )
    )
    (symbol "TSOP75_1_1"
      (pin power_in line (at -3.81 0 0) (length 2.225)
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "1" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at 2.54 5.08 270) (length 2.54)
        (name "V_S" (effects (font (size 1.27 1.27))))
        (number "2" (effects (font (size 1.27 1.27))))
      )
      (pin output line (at 2.54 -5.08 90) (length 2.54)
        (name "OUT" (effects (font (size 1.27 1.27))))
        (number "3" (effects (font (size 1.27 1.27))))
      )
      (pin power_in line (at -3.81 0 0) (length 2.225) hide
        (name "GND" (effects (font (size 1.27 1.27))))
        (number "4" (effects (font (size 1.27 1.27))))
      )
    )
  )
)
